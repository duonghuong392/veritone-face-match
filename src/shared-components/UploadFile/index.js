import React from 'react';
import { connect } from 'react-redux';
import { arrayOf, objectOf, shape, string, func, any, bool } from 'prop-types';

import RequestBar from 'shared-components/RequestBar';

/*import styles from './styles/index.scss';*/

import { uploadFileService } from 'modules/uploadFile';

class UploadFile extends React.Component {
  static propTypes = {
    uploadFileService: func.isRequired,
    /*steps: arrayOf(
      shape({
        name: string
      })
    ).isRequired,*/
    uploadStatus: string.isRequired,
    message: string.isRequired,
    // failureMessage: string.isRequired,
    //result: objectOf(any)
  };

  state = {
    expanded: false
  };

  handleFileUpload = (e, file) => {
   /* this.setState({
      expanded: true
    });
*/
    this.props.uploadFileService(file);
  };

  render() {
    const message = this.props.message

    return (
        <div><h4>Transcription Example</h4>
            <div className="index_requestBar__1jcZM">
                <div className="index_requestBar__header__147E-">
                    <div>
                        <div>{message}</div>
                    </div>
                    <div onClick={this.handleFileUpload}>
                        <input type="submit" className="index_FileInput__x3ffa" />
                            <label>
                                <span tabIndex="0" className="MuiButtonBase-root-15 MuiButton-root-1 MuiButton-raised-8 MuiButton-raisedPrimary-10" role="button">
                                    <span className="MuiButton-label-3">Upload</span>
                                    <span className="MuiTouchRipple-root-17"></span>
                                </span>
                            </label>
                    </div>
                </div><
                div className="index_container__2kfVP">
                <div></div>
            </div>
            </div>
        </div>
    );
  }
}

export default connect(
  state => ({
    ...state.uploadFile
  }),
  { uploadFileService }
)(UploadFile);
