import axios from 'axios';
import { helpers } from 'veritone-redux-common';
const { createReducer } = helpers;

export const UPLOAD_START = 'CLICK';
export const UPLOADING = 'UPLOADING';
export const UPLOAD_FAILURE = 'FAILURE';
export const UPLOAD_SUCCESS = 'SUCCESS';

export const namespace = 'uploadFile';

const defaultState = {
  uploadStatus: '',
  message: '',
  result: {}
};

const reducer = createReducer(defaultState, {
  [UPLOAD_START](state, action) {
    return {
      ...defaultState,
      uploadStatus: action.type,
      message: action.message,
    };
  },

  [UPLOADING](state, action) {
    return {
      ...state,
      uploadStatus: action.type,
      message: action.message,
    };
  },

  [UPLOAD_FAILURE](state, action) {
    return {
      ...state,
      uploadStatus: action.type,
      message: action.message,
    };
  },

  [UPLOAD_SUCCESS](state, action) {
    return {
      ...state,
      uploadStatus: action.type,
      message: action.message,
    };
  },
});

export default reducer;

export function uploadFileService(file) {
  return async (dispatch, getState, client) => {
    dispatch({ type: UPLOAD_START, message: 'UPLOAD_START' });
    setTimeout(function(){
        dispatch({ type: UPLOADING, message: 'UPLOADING' });
    }, 3000);

    setTimeout(function(){
        dispatch({ type: UPLOAD_SUCCESS, message: 'UPLOAD_SUCCESS' });
    }, 6000);

      /*setTimeout(function(){
          dispatch({ type: UPLOAD_FAILURE, message: 'UPLOAD_FAILURE' });
      }, 9000);*/

  };
}

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

/*async function pollForJob(jobId, client) {
  let job;
  try {
    let getJobQuery = `query {
      job(id:"${jobId}") {
        id
        status
        tasks {
          records {
            id
            status
            output
            target {
              id
            }
          }
        },
        target {
          id
        }
      }
    }
    `;
    await axios({
      url: '/v3/graphql',
      method: 'post',
      data: {
        query: getJobQuery  
      }
    }).then(response => {
      job = response.data.data.job;
    });
  } catch (e) {
    throw new Error('Failed to fetch job');
  }

  if (job.status === 'failed') {
    throw new Error('Job failed');
  }

  if (job.status === 'complete') {
    return job;
  }

  await delay(5000);
  return await pollForJob(jobId, client);
}*/
